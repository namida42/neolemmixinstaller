unit NLInstallItem;

interface

uses
  Generics.Collections, Generics.Defaults,
  NLInstallComms,
  Classes, SysUtils, StrUtils;

type

  { TNLContent }

  TNLContent = class
    private
      fTitle: String;
      fAuthor: String;
      fVersion: String;
      fDescription: String;
      fToInstall: Boolean;
      fFiles: TStringList;

      function GetListName: String;
    public
      constructor Create;
      destructor Destroy; override;

      property Title: string read fTitle write fTitle;
      property Author: string read fAuthor write fAuthor;
      property Version: string read fVersion write fVersion;
      property Description: string read fDescription write fDescription;
      property ToInstall: Boolean read fToInstall write fToInstall;
      property Files: TStringList read fFiles;
      property ListName: String read GetListName;
  end;

  { TNLContents }

  TNLContents = class(TObjectList<TNLContent>)
    private
      function CompareEntries(const Left, Right: TNLContent): Integer;
    public
      constructor Create;
  end;

  procedure MakeContentList(aDst: TNLContents);

implementation

procedure MakeContentList(aDst: TNLContents);
var
  SL: TStringList;

  NewContent: TNLContent;
  n: Integer;
begin
  aDst.Clear;
  SL := TStringList.Create;
  NewContent := nil;
  try
    GetContentList(SL);

    for n := 0 to SL.Count-1 do
    begin
      if (Trim(SL[n]) = '') or (n = SL.Count-1) or (LeftStr(Trim(SL[n]), 1) = '#') then
      begin
        if NewContent <> nil then
          aDst.Add(NewContent);

        NewContent := nil;
        Continue;
      end else if NewContent = nil then
        NewContent := TNLContent.Create;

      if Uppercase(SL.Names[n]) = 'TITLE' then
        NewContent.Title := SL.ValueFromIndex[n];

      if Uppercase(SL.Names[n]) = 'AUTHOR' then
        NewContent.Author := SL.ValueFromIndex[n];

      if Uppercase(SL.Names[n]) = 'DESCRIPTION' then
        NewContent.Description := SL.ValueFromIndex[n];

      if Uppercase(SL.Names[n]) = 'VERSION' then
        NewContent.Version := SL.ValueFromIndex[n];

      if Uppercase(SL[n]) = 'INSTALL' then
        NewContent.ToInstall := true;

      if Uppercase(SL.Names[n]) = 'FILE' then
        NewContent.Files.Add(SL.ValueFromIndex[n]);
    end;
  finally
    SL.Free;
  end;
end;

{ TNLContents }

function TNLContents.CompareEntries(const Left, Right: TNLContent): Integer;
begin
  if (Left.ToInstall and not Right.ToInstall) then
    Result := -1
  else if (Right.ToInstall and not Left.ToInstall) then
    Result := 1
  else
    Result := CompareStr(Left.Title, Right.Title);
end;

constructor TNLContents.Create;
begin
  inherited Create(TComparer<TNLContent>.Construct(CompareEntries), true);
end;

{ TNLContent }

function TNLContent.GetListName: String;
begin
  Result := fTitle;
  if fAuthor <> '' then Result := Result + ' (by ' + fAuthor + ')';

  if fToInstall then Result := '* ' + Result;
end;

constructor TNLContent.Create;
begin
  inherited;
  fFiles := TStringList.Create;
end;

destructor TNLContent.Destroy;
begin
  fFiles.Free;
  inherited Destroy;
end;

end.

