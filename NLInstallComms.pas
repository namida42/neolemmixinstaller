unit NLInstallComms;

interface

uses
  Dialogs,
  URLMon, ActiveX, Windows,
  Classes, SysUtils;

const
  VERSION_INFO_URL='https://www.neolemmix.com/installer/version.php';
  CONTENT_LIST_URL='https://www.neolemmix.com/installer/content.txt';

  procedure GetFileToFile(aUrl: String; aFile: String; aCallback: TProc<UInt64, UInt64> = nil);
  procedure GetFileToStream(aUrl: String; aStream: TStream; aCallback: TProc<UInt64, UInt64> = nil);
  procedure GetFileToStringList(aUrl: String; aStringList: TStringList; aCallback: TProc<UInt64, UInt64> = nil);
  procedure GetVersionInfo(aStringList: TStringList);
  procedure GetContentList(aStringList: TStringList);

  function AppPath: String; // meh, don't want to make a dedicated file just for this

implementation

type
  TCallback = class(TInterfacedObject, IBindStatusCallback)
    private
      fCallbackMethod: TProc<UInt64, UInt64>;
    public
      constructor Create(aCallback: TProc<UInt64, UInt64>);

      function OnStartBinding(dwReserved: DWORD; pib: IBinding): HResult; stdcall;
      function GetPriority(out nPriority): HResult; stdcall;
      function OnLowResource(reserved: DWORD): HResult; stdcall;
      function OnProgress(ulProgress: ULONG; ulProgressMax, ulStatusCode: ULONG; szStatusText: PWideChar): HResult; stdcall;
      function OnStopBinding(hresult: HResult; szError: PWideChar): HResult; stdcall;
      function GetBindInfo(out grfBINDF: TBindF; var pbindinfo: TBindInfo): HResult; stdcall;
      function OnDataAvailable(grfBSCF: TBSCF; dwSize: DWORD; pformatetc: PFormatEtc; pstgmed: PStgMedium): HResult; stdcall;
      function OnObjectAvailable(const iid: TGUID; punk: IUnknown): HResult; stdcall;
  end;

function AppPath: String;
begin
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
end;

procedure GetFileToFile(aUrl: String; aFile: String; aCallback: TProc<UInt64, UInt64> = nil);
var
  HR: HResult;
begin
  HR := URLDownloadToFile(nil, PChar(aUrl), PChar(aFile), 0, TCallback.Create(aCallback));
  if HR <> S_OK then
    raise Exception.Create('Result 0x' + IntToHex(HR, 8) + ', error 0x' + IntToHex(GetLastError, 8) + ' downloading: ' + aUrl);
end;

procedure GetFileToStream(aUrl: String; aStream: TStream; aCallback: TProc<UInt64, UInt64> = nil);
var
  MS: TMemoryStream;
begin
  GetFileToFile(aUrl, AppPath + 'temp', aCallback);

  MS := TMemoryStream.Create;
  try
    MS.LoadFromFile(AppPath + 'temp');
    DeleteFile(AppPath + 'temp');
    MS.Position := 0;
    aStream.CopyFrom(MS, MS.Size);
  finally
    MS.Free;
  end;
end;

{
procedure GetFileToStream(aUrl: String; aStream: TStream);
const
  CHUNK_SIZE = 512;
var
  TempStream: PIStream;

  BytesRead: Cardinal;

  Buffer: packed array[0..CHUNK_SIZE-1] of byte;
begin
  TempStream := nil;
  if not (URLOpenBlockingStream(nil, PAnsiChar(aURL), TempStream, 0, nil) <> Windows.S_OK) then
    raise Exception.Create('URL stream open failed, error code ' + IntToHex(Windows.GetLastError, 8));

  try
    repeat
      TempStream^.Read(@Buffer[0], 0, @BytesRead);
      aStream.Write(Buffer[0], BytesRead);
    until BytesRead < CHUNK_SIZE;
  finally
    TempStream^._Release;
  end;
end;
}

procedure GetFileToStringList(aUrl: String; aStringList: TStringList; aCallback: TProc<UInt64, UInt64> = nil);
var
  MS: TMemoryStream;
begin
  MS := TMemoryStream.Create;
  try
    GetFileToStream(aUrl, MS, aCallback);
    MS.Position := 0;
    aStringList.LoadFromStream(MS);
  finally
    MS.Free;
  end;

  if aStringList.Names[0] = '#REDIRECT' then
    GetFileToStringList(aStringList.ValueFromIndex[0], aStringList);
end;

procedure GetVersionInfo(aStringList: TStringList);
begin
  GetFileToStringList(VERSION_INFO_URL, aStringList);
end;

procedure GetContentList(aStringList: TStringList);
begin
  GetFileToStringList(CONTENT_LIST_URL, aStringList);
end;

{ TCallback }

constructor TCallback.Create(aCallback: TProc<UInt64, UInt64>);
begin
  inherited Create;
  fCallbackMethod := aCallback;
end;

function TCallback.GetBindInfo(out grfBINDF: TBindF;
  var pbindinfo: TBindInfo): HResult;
begin
  Result := 0;
end;

function TCallback.GetPriority(out nPriority): HResult;
begin
  Result := 0;
end;

function TCallback.OnDataAvailable(grfBSCF: TBSCF; dwSize: DWORD;
  pformatetc: PFormatEtc; pstgmed: PStgMedium): HResult;
begin
  Result := 0;
end;

function TCallback.OnLowResource(reserved: DWORD): HResult;
begin
  Result := 0;
end;

function TCallback.OnObjectAvailable(const iid: TGUID;
  punk: IInterface): HResult;
begin
  Result := 0;
end;

function TCallback.OnProgress(ulProgress, ulProgressMax, ulStatusCode: ULONG;
  szStatusText: PWideChar): HResult;
begin
  if (ulProgressMax > 0) and Assigned(fCallbackMethod) then
    fCallbackMethod(ulProgress, ulProgressMax);

  Result := 0;
end;

function TCallback.OnStartBinding(dwReserved: DWORD; pib: IBinding): HResult;
begin
  Result := 0;
end;

function TCallback.OnStopBinding(hresult: HResult; szError: PWideChar): HResult;
begin
  Result := 0;
end;

end.

