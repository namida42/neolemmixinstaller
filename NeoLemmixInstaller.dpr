program NeoLemmixInstaller;

uses
  Vcl.Forms,
  NLInstallMain in 'NLInstallMain.pas' {Form1},
  NLInstallComms in 'NLInstallComms.pas',
  NLInstallItem in 'NLInstallItem.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFNLInstall, FNLInstall);
  Application.Run;
end.
