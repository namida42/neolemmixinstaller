unit NLInstallMain;

interface

uses
  NLInstallComms, NLInstallItem, Zip, Windows,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Vcl.ExtCtrls;

const
  NEOLEMMIX_URL = 'https://www.neolemmix.com/download.php?program=16*NeoLemmix.zip';
  EDITOR_URL = 'https://www.neolemmix.com/download.php?program=17*NLEditor.exe';
  STYLES_URL = 'https://www.neolemmix.com/download.php?program=52*styles.zip';

type

  { TFNLInstall }

  TFNLInstall = class(TForm)
    btnInstall: TButton;
    lblInstallStatus: TLabel;
    panDownloadSettings: TPanel;
    lblLevelPacks: TLabel;
    ebNLVersion: TEdit;
    ebEditorVersion: TEdit;
    ebStylesVersion: TEdit;
    cbNeoLemmix: TCheckBox;
    cbEditor: TCheckBox;
    cbStyles: TCheckBox;
    lbPacks: TListBox;
    cbInstall: TCheckBox;
    gbPackInfo: TGroupBox;
    lblPackTitleAuthor: TLabel;
    lblPackVersion: TLabel;
    lblPackDescription: TLabel;
    procedure btnInstallClick(Sender: TObject);
    procedure cbInstallClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbPacksSelectionChange(Sender: TObject);
  private
    fContents: TNLContents;
    fDownloading: Boolean;

    procedure UpdateContentList;
    procedure DownloadCallback(Prog, MaxProg: UInt64);
  public

  end;

var
  FNLInstall: TFNLInstall;

implementation

{$R *.dfm}

{ TFNLInstall }

procedure TFNLInstall.cbInstallClick(Sender: TObject);
var
  n: Integer;
  ThisContent: TNLContent;
begin
  for n := 0 to lbPacks.Count-1 do
  begin
    if not lbPacks.Selected[n] then Continue;
    ThisContent := TNLContent(lbPacks.Items.Objects[n]);
    ThisContent.ToInstall := cbInstall.Checked;
  end;

  UpdateContentList;
end;

var
  TextBase: String;

procedure TFNLInstall.DownloadCallback(Prog, MaxProg: UInt64);
  function MakeFileSizeString(Size: UInt64): String;
  const
    CUTOVER_SIZE = 16;
  begin
    if Size < (CUTOVER_SIZE * 1024) then
      Result := IntToStr(Size) + 'B'
    else if Size < (CUTOVER_SIZE * 1024 * 1024) then
      Result := IntToStr(Size div 1024) + 'KB'
    else
      Result := IntToStr(Size div (1024 * 1024)) + 'MB';
  end;
begin
  if MaxProg = 0 then
    lblInstallStatus.Caption := TextBase + ' (? / ?)'
  else
    lblInstallStatus.Caption := TextBase + ' (' +
                                MakeFileSizeString(Prog) + ' / ' +
                                MakeFileSizeString(MaxProg) + ')';

  Application.ProcessMessages;
end;

procedure TFNLInstall.btnInstallClick(Sender: TObject);
var
  SrcFiles: TStringList;
  DstFiles: TStringList;
  FailedFiles: TStringList;
  TempSL: TStringList;
  BaseNewFilename, NewFilename: String;
  n, i: Integer;
  Zip: TZipFile;

  function LeadZeroStr(aVal: Integer; aLen: Integer): String;
  begin
    Result := IntToStr(aVal);
    while Length(Result) < aLen do
      Result := '0' + Result;
  end;

  function ProgStr(aCur, aMax: Integer): String;
  begin
    Result := IntToStr(aCur) + '/' + IntToStr(aMax);
  end;

begin
  panDownloadSettings.Enabled := false;
  btnInstall.Enabled := false;
  fDownloading := true;
  try
    SrcFiles := TStringList.Create;
    DstFiles := TStringList.Create;
    FailedFiles := TStringList.Create;
    TempSL := TStringList.Create;
    try
      TempSL.Delimiter := '*';
      TempSL.StrictDelimiter := true;

      if cbNeoLemmix.Checked then SrcFiles.Add(NEOLEMMIX_URL);
      if cbEditor.Checked then SrcFiles.Add(EDITOR_URL);
      if cbStyles.Checked then SrcFiles.Add(STYLES_URL);
      for n := 0 to fContents.Count-1 do
        if fContents[n].ToInstall then
          for i := 0 to fContents[n].Files.Count-1 do
            SrcFiles.Add(fContents[n].Files[i]);

      ForceDirectories(AppPath + 'installer-temp\');

      for i := 0 to SrcFiles.Count-1 do
        try
          NewFilename := '????';
          TempSL.DelimitedText := SrcFiles[i];
          NewFilename := AppPath + 'installer-temp\' + TempSL[1];
          BaseNewFilename := NewFilename;

          n := 1;
          while FileExists(NewFilename) do
          begin
            NewFilename := ChangeFileExt(BaseNewFilename, '') + '.' + LeadZeroStr(n, 3) + ExtractFileExt(BaseNewFilename);
            Inc(n);
          end;

          TextBase := 'Downloading ' + ProgStr(i + 1, SrcFiles.Count)  + ': ' + ExtractFilename(NewFilename);
          GetFileToFile(TempSL[0], NewFilename, DownloadCallback);
          DstFiles.Add(NewFilename);
        except
          on E: Exception do
          begin
            FailedFiles.Add(SrcFiles[i]);
            lblInstallStatus.Caption := 'Failed ' + ProgStr(i + 1, SrcFiles.Count)  + ': ' + ExtractFilename(NewFilename);
            Application.ProcessMessages;
            Sleep(200);
          end;
        end;

      for i := 0 to DstFiles.Count-1 do
      begin
        if (Uppercase(ExtractFileExt(DstFiles[i])) = '.ZIP') or (Uppercase(ExtractFileExt(DstFiles[i])) = '.MZIP') or
           (Uppercase(ExtractFileExt(DstFiles[i])) = '.LZIP') or (Uppercase(ExtractFileExt(DstFiles[i])) = '.NZIP') then
        begin
          lblInstallStatus.Caption := 'Extracting ' + ProgStr(i + 1, DstFiles.Count)  + ': ' + ExtractFilename(DstFiles[i]);
          Application.ProcessMessages;

          Zip := TZipFile.Create;
          try
            Zip.Open(DstFiles[i], zmRead);
            if (Uppercase(ExtractFileExt(DstFiles[i])) = '.MZIP') then
              Zip.ExtractAll(AppPath + 'music\')
            else if (Uppercase(ExtractFileExt(DstFiles[i])) = '.LZIP') then
              Zip.ExtractAll(AppPath + 'levels\')
            else if (Uppercase(ExtractFileExt(DstFiles[i])) = '.NZIP') then
              Zip.ExtractAll(AppPath + 'levels\' + ChangeFileExt(ExtractFileName(DstFiles[i]), '') + '\')
            else
              Zip.ExtractAll(AppPath);
          finally
            Zip.Free;
          end;

          DeleteFile(DstFiles[i]);
        end else begin
          lblInstallStatus.Caption := 'Moving ' + ProgStr(i + 1, DstFiles.Count)  + ': ' + ExtractFilename(DstFiles[i]);
          Application.ProcessMessages;
          if FileExists(AppPath + ExtractFileName(DstFiles[i])) then
            DeleteFile(AppPath + ExtractFileName(DstFiles[i]));
          RenameFile(DstFiles[i], AppPath + ExtractFileName(DstFiles[i]));
        end;
      end;

      SetCurrentDir(AppPath);
      RemoveDir(AppPath + 'installer-temp');

      lblInstallStatus.Caption := 'Installation complete.';
    finally
      SrcFiles.Free;
      DstFiles.Free;
      FailedFiles.Free;
      TempSL.Free;
    end;
  finally
    fDownloading := false;
    btnInstall.Enabled := true;
    panDownloadSettings.Enabled := true;
  end;
end;

procedure TFNLInstall.FormShow(Sender: TObject);
var
  VersionSL: TStringList;
begin
  VersionSL := TStringList.Create;
  try
    GetVersionInfo(VersionSL);
    ebNLVersion.Text := VersionSL.Values['game'];
    ebEditorVersion.Text := VersionSL.Values['editor'];
    ebStylesVersion.Text := VersionSL.Values['styles'];
  finally
    VersionSL.Free;
  end;

  MakeContentList(fContents);
  UpdateContentList;
end;

procedure TFNLInstall.lbPacksSelectionChange(Sender: TObject);
var
  CheckState: Integer; // 1 = unchecked, 2 = greyed, 3 = set, 0 = not yet set
  n: Integer;
  ThisContent: TNLContent;
begin
  for n := 0 to gbPackInfo.ControlCount-1 do
    gbPackInfo.Controls[n].Visible := lbPacks.SelCount = 1;

  case lbPacks.SelCount of
    0: gbPackInfo.Caption := 'No pack selected';
    1: begin
         ThisContent := TNLContent(lbPacks.Items.Objects[lbPacks.ItemIndex]);
         lblPackTitleAuthor.Caption := ThisContent.Title;
         if ThisContent.Author <> '' then lblPackTitleAuthor.Caption := lblPackTitleAuthor.Caption + ' (by ' + ThisContent.Author + ')';

         if ThisContent.Version = '' then
           lblPackVersion.Caption := ''
         else
           lblPackVersion.Caption := 'Version: ' + ThisContent.Version;

         lblPackDescription.Caption := ThisContent.Description;
       end;
    else gbPackInfo.Caption := 'Multiple packs selected';
  end;

  CheckState := 0;
  for n := 0 to lbPacks.Count-1 do
  begin
    if not lbPacks.Selected[n] then Continue;

    ThisContent := TNLContent(lbPacks.Items.Objects[n]);

    if ThisContent.ToInstall then
    begin
      if CheckState in [0, 3] then
        CheckState := 3
      else
        CheckState := 2;
    end else begin
      if CheckState in [0, 1] then
        CheckState := 1
      else
        CheckState := 2;
    end;
  end;

  cbInstall.OnClick := nil;
  try
    case CheckState of
      0: begin cbInstall.State := cbUnchecked; cbInstall.Enabled := false; end;
      1: begin cbInstall.State := cbUnchecked; cbInstall.Enabled := true; end;
      2: begin cbInstall.State := cbGrayed; cbInstall.Enabled := true; end;
      3: begin cbInstall.State := cbChecked; cbInstall.Enabled := true; end;
    end;
  finally
    cbInstall.OnClick := cbInstallClick;
  end;
end;

procedure TFNLInstall.UpdateContentList;
var
  n: Integer;
begin
  lbPacks.ItemIndex := -1;
  fContents.Sort;

  for n := 0 to fContents.Count-1 do
  begin
    if n < lbPacks.Count then
    begin
      lbPacks.Items[n] := fContents[n].ListName;
      lbPacks.Items.Objects[n] := fContents[n];
    end else
      lbPacks.AddItem(fContents[n].ListName, fContents[n]);
  end;

  while lbPacks.Count > fContents.Count do
    lbPacks.Items.Delete(lbPacks.Count-1);
end;

procedure TFNLInstall.FormCreate(Sender: TObject);
begin
  fContents := TNLContents.Create;
end;

procedure TFNLInstall.FormDestroy(Sender: TObject);
begin
  fContents.Free;
end;

end.

