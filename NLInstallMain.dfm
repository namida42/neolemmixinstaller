object FNLInstall: TFNLInstall
  Left = 477
  Top = 118
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'NeoLemmix Installer'
  ClientHeight = 543
  ClientWidth = 322
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    322
    543)
  PixelsPerInch = 96
  TextHeight = 13
  object lblInstallStatus: TLabel
    Left = 8
    Top = 488
    Width = 304
    Height = 13
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    ExplicitWidth = 269
  end
  object btnInstall: TButton
    Left = 109
    Top = 510
    Width = 110
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Install'
    TabOrder = 0
    OnClick = btnInstallClick
    ExplicitTop = 507
    ExplicitWidth = 75
  end
  object panDownloadSettings: TPanel
    Left = 0
    Top = 0
    Width = 322
    Height = 476
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ExplicitWidth = 287
    ExplicitHeight = 473
    DesignSize = (
      322
      476)
    object lblLevelPacks: TLabel
      Left = 8
      Top = 104
      Width = 99
      Height = 13
      Caption = 'Select Level Packs    '
      Color = clBtnFace
      ParentColor = False
    end
    object ebNLVersion: TEdit
      Left = 136
      Top = 12
      Width = 175
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ReadOnly = True
      TabOrder = 0
      ExplicitWidth = 140
    end
    object ebEditorVersion: TEdit
      Left = 136
      Top = 40
      Width = 175
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ReadOnly = True
      TabOrder = 1
      ExplicitWidth = 140
    end
    object ebStylesVersion: TEdit
      Left = 136
      Top = 68
      Width = 175
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      ReadOnly = True
      TabOrder = 2
      ExplicitWidth = 140
    end
    object cbNeoLemmix: TCheckBox
      Left = 8
      Top = 14
      Width = 124
      Height = 20
      Caption = 'NeoLemmix Engine'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object cbEditor: TCheckBox
      Left = 8
      Top = 42
      Width = 119
      Height = 20
      Caption = 'NeoLemmix Editor'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object cbStyles: TCheckBox
      Left = 8
      Top = 70
      Width = 51
      Height = 20
      Caption = 'Styles'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object lbPacks: TListBox
      Left = 8
      Top = 128
      Width = 303
      Height = 184
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      MultiSelect = True
      TabOrder = 6
      OnClick = lbPacksSelectionChange
      ExplicitWidth = 268
    end
    object cbInstall: TCheckBox
      Left = 16
      Top = 316
      Width = 140
      Height = 20
      Anchors = [akLeft, akBottom]
      Caption = 'Install Selected Pack(s)'
      TabOrder = 7
      OnClick = cbInstallClick
    end
    object gbPackInfo: TGroupBox
      Left = 8
      Top = 336
      Width = 304
      Height = 132
      Anchors = [akLeft, akRight, akBottom]
      Caption = 'No Pack Selected'
      TabOrder = 8
      ExplicitWidth = 269
      DesignSize = (
        304
        132)
      object lblPackTitleAuthor: TLabel
        Left = 7
        Top = 17
        Width = 283
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Color = clBtnFace
        ParentColor = False
        Visible = False
        ExplicitWidth = 248
      end
      object lblPackVersion: TLabel
        Left = 7
        Top = 33
        Width = 283
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Color = clBtnFace
        ParentColor = False
        Visible = False
        ExplicitWidth = 248
      end
      object lblPackDescription: TLabel
        Left = 7
        Top = 53
        Width = 283
        Height = 68
        Anchors = [akLeft, akTop, akRight, akBottom]
        AutoSize = False
        Color = clBtnFace
        ParentColor = False
        Visible = False
        WordWrap = True
        ExplicitWidth = 248
        ExplicitHeight = 72
      end
    end
  end
end
